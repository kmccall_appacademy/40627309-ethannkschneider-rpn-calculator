class RPNCalculator
  def initialize
    @stack = []
  end

  def push(num)
    @stack.push(num)
  end

  def plus
    raise "calculator is empty" if @stack.size < 2
    a = @stack.pop
    b = @stack.pop
    push(a + b)
  end

  def value
    @stack[-1]
  end

  def minus
    raise "calculator is empty" if @stack.size < 2
    a = @stack.pop
    b = @stack.pop
    push(b - a)
  end

  def divide
    raise "calculator is empty" if @stack.size < 2
    a = @stack.pop
    b = @stack.pop
    push(b.to_f / a.to_f)
  end

  def times
    raise "calculator is empty" if @stack.size < 2
    a = @stack.pop
    b = @stack.pop
    push(b * a)
  end

  def tokens(string)
    numbers = "0123456789"
    result = []
    string.split.each do |el|
      if numbers.include?(el)
        result << el.to_i
      else
        result << el.to_sym
      end
    end

    result
  end

  def evaluate(string)
    @stack = []
    tokens(string).each do |el|
      case el
      when Integer
        push(el)
      when Symbol
        plus if el == :+
        minus if el == :-
        divide if el == :/
        times if el == :*
      else
        raise "This operator isn't in the calculator!"
      end
    end

    return value if @stack.size == 1
    raise "Bad input no good!"
  end

end
